package e.extreme.practicapm45;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity implements View.OnClickListener,FrgUno.OnFragmentInteractionListener,FrgDos.OnFragmentInteractionListener {

    Button botonFragUno, botonFragDos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);
        botonFragUno = (Button) findViewById(R.id.btnFrgUno);
        botonFragDos = (Button) findViewById(R.id.btnFrgDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFrgUno:
                FrgUno fragmentoUno = new FrgUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contebedor, fragmentoUno);
                transactionUno.commit();
                break;

            case R.id.btnFrgDos:
                FrgDos fragmentoDos = new FrgDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contebedor, fragmentoDos);
                transactionDos.commit();


        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(ActividadPrincipal.this);
                dialogoLogin.setContentView(R.layout.dlg_login);
                Button botonAutenticar=(Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario =(EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogoLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadPrincipal.this,cajaUsuario.getText().toString()+" "+ cajaClave.getText().toString(),Toast.LENGTH_LONG);
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
               Dialog dialogoRegistrar = new Dialog(ActividadPrincipal.this);
               dialogoRegistrar.setContentView(R.layout.dlg_registrar);
               Button botonRegistrar=(Button) dialogoRegistrar.findViewById(R.id.btnRegistrar2);
               final EditText cajaNombre = (EditText) dialogoRegistrar.findViewById(R.id.txtNombre);
               final EditText cajaApellido = (EditText) dialogoRegistrar.findViewById(R.id.txtApellido);

               botonRegistrar.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       Toast.makeText(ActividadPrincipal.this,cajaNombre.getText().toString()+" "+ cajaApellido.getText().toString(),Toast.LENGTH_LONG);
                   }
               });
               dialogoRegistrar.show();
                break;
        }
        return true;
    }
}



